-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore
    = math, minetest, nodecore
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local basename = modname .. ":starflower"
local nerfsuff = "_nerf"

local function reg(suff, light_source)
	return minetest.register_node(basename .. suff, {
			description = "Pinnacle Flower",
			drawtype = 'plantlike',
			waving = 1,
			tiles = {modname .. "_starflower.png"},
			wield_image = modname .. "_starflower.png",
			inventory_image = modname .. "_starflower.png",
			sunlight_propagates = true,
			paramtype = 'light',
			light_source = light_source,
			walkable = false,
			groups = {
				snappy = 1,
				flora = 1,
				flammable = 1,
				attached_node = 1,
				decay_to_fibers = 1,
				pinnacle_flower = 1,
			},
			sounds = nodecore.sounds("nc_terrain_swishy"),
			selection_box = {
				type = "fixed",
				fixed = {-6/16, -0.5, -6/16, 6/16, 4/16, 6/16},
			},
			mapcolor = {r = 180, g = 170, b = 195, a = 128}
		})
end
reg("", 7)
reg(nerfsuff, 3)

local function lightcheck(pos)
	if nodecore.is_full_sun(pos) then
		return basename
	end
	return basename .. nerfsuff
end

nodecore.register_abm({
		label = "starflower light level",
		nodenames = {"group:pinnacle_flower"},
		interval = 1,
		chance = 5,
		action = function(pos, node)
			local nn = lightcheck(pos)
			if nn == node.name then return end
			node.name = nn
			return minetest.set_node(pos, node)
		end
	})
nodecore.register_aism({
		label = "starflower light level",
		itemnames = {"group:pinnacle_flower"},
		interval = 1,
		chance = 5,
		action = function(stack, data)
			local nn = lightcheck(data.pos)
			if nn == stack:get_name() then return end
			stack:set_name(nn)
			return stack
		end
	})

minetest.register_decoration({
		label = {modname .. ":starflower"},
		deco_type = "simple",
		place_on = {"group:soil"},
		sidelen = 16,
		fill_ratio = 0.02,
		noise_params = {
			offset = -0.001 + 0.005 * 0.0005,
			scale = 0.001,
			spread = {x = 100, y = 100, z = 100},
			seed = 1572,
			octaves = 3,
			persist = 0.7
		},
		y_max = 31000,
		y_min = 100,
		decoration = {modname .. ":starflower"},
	})

minetest.register_decoration({
		label = {modname .. ":starflower"},
		deco_type = "simple",
		place_on = {"group:soil"},
		sidelen = 16,
		fill_ratio = 0.02,
		noise_params = {
			offset = -0.001 + 0.005 * 0.005,
			scale = 0.001,
			spread = {x = 100, y = 100, z = 100},
			seed = 1572,
			octaves = 3,
			persist = 0.7
		},
		y_max = 31000,
		y_min = 200,
		decoration = {modname .. ":starflower"},
	})

local function pinnacle_fertile(pos)
	if not nodecore.nature_fertile(pos) then return end

	-- Pinnacle flowers require an extreme energy source to reproduce.
	if #nodecore.find_nodes_around(pos, "group:lux_cobble_max", 1) < 1 then return end

	return true
end
minetest.register_abm({
		label = "nature starflower spreading",
		nodenames = {"group:pinnacle_flower"},
		interval = 12,
		chance = 10,
		action = function(pos, node)
			if not pinnacle_fertile(pos) then return end
			local gro = {
				x = pos.x + math_random(-1, 1),
				y = pos.y + math_random(-1, 1),
				z = pos.z + math_random(-1, 1)
			}
			if not pinnacle_fertile(gro, node) then return end

			-- Pinnacle flowers have much higher self-inhibition than
			-- common nature spreaders.
			if #nodecore.find_nodes_around(pos, "group:pinnacle_flower", 4) >= 3
			then return end

			return nodecore.set_loud(gro, node)
		end
	})
