-- LUALOCALS < ---------------------------------------------------------
local include
    = include
-- LUALOCALS > ---------------------------------------------------------

include("feature_biomes")
include("feature_dungeon")
include("feature_spread")

include("item_fiber")
include("item_thatch")

include("decor_boulder")
include("decor_decaylog")
include("decor_tree_antique")
include("decor_tree_grand")
include("decor_tree_tall")

include("flora_bamboo")
include("flora_fern")
include("flora_flower")
include("flora_grass")
include("flora_lilypad")
include("flora_mossy")
include("flora_mushroom")
include("flora_reed")
include("flora_shrub")
include("flora_starflower")
include("flora_thornbriar")
