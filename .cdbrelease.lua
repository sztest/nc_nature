-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "nc_nature",
	version = stamp .. "-$Format:%h$",
	type = "mod",
	dev_state = "ACTIVELY_DEVELOPED",
	title = "NodeCore Nature: Revised",
	short_description = "Updated, fixed, enhanced edition of Winter94's NodeCore Nature",
	tags = {"building", "decorative", "mapgen", "plants_and_farming"},
	license = "GPL-3.0-only",
	media_license = "GPL-3.0-only",
	repo = "https://gitlab.com/sztest/nc_nature",
	issue_tracker = "https://discord.gg/NNYeF6f",
	long_description = readtext('README.md'),
	screenshots = {
		readbinary('.cdb-screen1.webp'),
		readbinary('.cdb-screen2.webp'),
		readbinary('.cdb-screen3.webp'),
		readbinary('.cdb-screen4.webp'),
		readbinary('.cdb-screen5.webp'),
	}
}

-- luacheck: pop
